public struct TestingSPM {
    public private(set) var text = "Hello, World!"

    public init() {
    }
    
    public func reply() -> String {
        return "Hello there."
    }
}
